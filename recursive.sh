function recursive_for {
    level=$1
    echo "Level: ${level}"
    if [ $level -ge 501 ]; then
        exit;
    fi ;
    let "level=level+1"
    ls -1| while read f; do
        if [ -d "$f"  -a ! -h "$f" ];
        then
            cd -- "$f";
            while [ "$(ps -aux -U $USER | wc -l)" -ge 32 ] ; do sleep 1 ; done
            DIR=`pwd`
            ( echo "${DIR} -exec touch -amh '{}' + ";
              find ${DIR} -exec touch -amh '{}' + )  &
            # while [ "$(ps -aux -U $USER | wc -l)" -ge 32 ] ; do sleep 1 ; done
            # use recursion to navigate the entire tree
            recursive_for ${level};
            cd ..;
        fi;
    done;
};
#cd /scratch/fp0/mah900/
cd /scratch/fp0/mah900/tiny-imagenet-200/
recursive_for 0
wait
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT
